# Ron Swanson Quotes

Welcome to the Ron Swanson Quotes project! This application showcases some of the memorable quotes from the iconic character Ron Swanson from the TV show "Parks and Recreation".

## Getting Started

   git clone https://gitlab.com/shreshthsharma800/ronquotes.git

   cd ronquotes

   npm install

   npm start


## Technologies

- React.js
- React Router DOM
- Tailwind CSS
- Vercel (for deployment)

## Deployment

This project is deployed using Vercel. Any changes pushed to the main branch will automatically trigger a new deployment.

