import React, { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { QuotesContext } from "../../App";
import CardMain from "../../components/Card";

const Favorites = () => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setVisible(true);
  }, []);

  const { list } = useContext(QuotesContext);
  const [currentPage, setCurrentPage] = useState(1);
  const quotesPerPage = 6;

  const totalPages = Math.ceil(list.length / quotesPerPage);

  const currentQuotes = list.slice(
    (currentPage - 1) * quotesPerPage,
    currentPage * quotesPerPage
  );

  useEffect(() => {
    if (currentPage > totalPages) {
      setCurrentPage(totalPages);
    }
  }, [list, totalPages, currentPage]);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <div className="flex flex-col justify-center items-center h-auto pb-5 pt-8">
      <Link
        to="/"
        className="ease-in-out relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-pink-500 to-orange-400 group-hover:from-pink-500 group-hover:to-orange-400 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800"
      >
        <span className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
          Back To Quotes
        </span>
      </Link>
      <h1
        className={`text-5xl transition-opacity duration-700 ease-in-out bg-gradient-to-r from-cyan-500 to-blue-500 font-pacifico mb-4 mt-10 font-extrabold leading-none tracking-tight text-transparent bg-clip-text md:text-5xl lg:text-8xl p-8 ${
          visible ? "opacity-100" : "opacity-0"
        }`}
      >
        Favorite Quotes
      </h1>
      <div className="grid grid-cols-1 gap-6 lg:grid-cols-2 xl:grid-cols-3">
        {currentQuotes.length === 0 ? (
          <p className="text-gray-600">No favorite quotes yet.</p>
        ) : (
          currentQuotes.map((quote, index) => (
            <CardMain key={index} text={quote} isVisible={true} />
          ))
        )}
      </div>
      <nav aria-label="Page navigation example">
        <ul className="inline-flex -space-x-px text-sm mt-4">
          <li>
            <button
              onClick={() => handlePageChange(currentPage - 1)}
              disabled={currentPage === 1}
              className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
              Previous
            </button>
          </li>
          {Array.from({ length: totalPages }, (_, index) => (
            <li key={index}>
              <button
                onClick={() => handlePageChange(index + 1)}
                className={`flex items-center justify-center px-3 h-8 leading-tight ${
                  currentPage === index + 1
                    ? "text-blue-600 border border-gray-300 bg-blue-50 hover:bg-blue-100 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white"
                    : "text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                }`}
              >
                {index + 1}
              </button>
            </li>
          ))}
          <li>
            <button
              onClick={() => handlePageChange(currentPage + 1)}
              disabled={currentPage === totalPages}
              className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
              Next
            </button>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Favorites;
