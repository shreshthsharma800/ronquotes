import { useCallback, useEffect, useRef, useState, useContext } from "react";
import { QuotesContext } from "../../App";
import { Link } from "react-router-dom";
import CardMain from "../../components/Card";
import Header from "../../components/Header";

const QuotesMain = () => {
  const { list, setList } = useContext(QuotesContext);
  const [quote, setQuote] = useState();
  const [loading, setLoading] = useState(true);
  const timeOutRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    setIsVisible(true);
    if (timeOutRef.current) {
      clearTimeout(timeOutRef.current);
    }
    const fetchData = async () => {
      setIsVisible(false);
      try {
        const Quote = await fetch(
          "https://ron-swanson-quotes.herokuapp.com/v2/quotes"
        );
        const QuoteJson = await Quote.json();
        setLoading(false);
        setQuote(QuoteJson);
        setIsVisible(true);
      } catch (err) {
        console.error("Got an error", err);
      }
    };
    timeOutRef.current = setTimeout(fetchData, 4000);
    return () => clearTimeout(timeOutRef.current);
  }, [quote]);

  const addToList = useCallback(() => {
    if (!list.find((listQuote) => listQuote === quote) && quote !== undefined) {
      setList((prevList) => [...prevList, quote]);
    }
  }, [list, quote]);

  useEffect(() => {
    console.log(list);
  }, [list]);

  return (
    <div className="flex flex-col justify-center items-center h-auto">
      <Header />
      {loading ? (
        <CardMain text="Loading..." isVisible={isVisible} />
      ) : (
        <CardMain text={quote} isVisible={isVisible} />
      )}
      <div className="flex flex-row justify-center align-baseline mt-7">
        <Link
          to="/favorites"
          className="ease-in-out relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-pink-500 to-orange-400 group-hover:from-pink-500 group-hover:to-orange-400 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800"
        >
          <span className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
            List Of Favourites
          </span>
        </Link>
        <button
          onClick={addToList}
          className="ease-in-out relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-pink-500 to-orange-400 group-hover:from-pink-500 group-hover:to-orange-400 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800"
        >
          <span className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
            Add To Favourites
          </span>
        </button>
      </div>
    </div>
  );
};
export default QuotesMain;
