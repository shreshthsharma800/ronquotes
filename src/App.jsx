import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import QuotesMain from "./pages/QuotesMain/QuotesMain";
import Default from "./data/RonData,js";
import Favorites from "./pages/Favorites/Favorites";

export const QuotesContext = React.createContext();

function App() {
  const vw = window.innerWidth;
  console.log(vw);
  const [list, setList] = useState(Default);

  return (
    <QuotesContext.Provider value={{ list, setList }}>
      <Router>
        <Routes>
          <Route path="/" element={<QuotesMain />} />
          <Route path="/favorites" element={<Favorites />} />
        </Routes>
      </Router>
    </QuotesContext.Provider>
  );
}

export default App;
