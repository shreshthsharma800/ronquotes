import RonImg from "../assets/Ron.svg";

export default function CardMain({ text, isVisible }) {
  return (
    <div className="flex-1 min-h-52 min-w-96 m-5 max-w-sm bg-white border-2 border-gray-300 p-6 rounded-md tracking-wide shadow-lg">
      <div className="flex items-center mb-4">
        <img
          alt="avatar"
          className="w-20 rounded-full border-2 border-gray-300"
          src={RonImg}
        />
        <div className="leading-5 ml-6">
          <h4 className="text-xl font-semibold">Ron Swanson</h4>
          <h5 className="font-semibold text-blue-600">
            Director - P&R Department
          </h5>
        </div>
      </div>
      <div
        className={`transition-opacity duration-500 ease-in-out ${
          isVisible ? "opacity-100" : "opacity-0"
        }`}
      >
        <q className="italic text-gray-600">{text}</q>
      </div>
    </div>
  );
}
