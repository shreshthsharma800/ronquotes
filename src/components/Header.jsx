import { useEffect, useState } from "react";

const Header = () => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setVisible(true);
  }, []);
  return (
    <h1
      className={`text-5xl transition-opacity duration-700 ease-in-out bg-gradient-to-r from-cyan-500 to-blue-500 font-pacifico mb-4 mt-10 font-extrabold leading-none tracking-tight text-transparent bg-clip-text md:text-5xl lg:text-8xl p-4 ${
        visible ? "opacity-100" : "opacity-0"
      }`}
    >
      Awesome Ron
    </h1>
  );
};

export default Header;
